package com.example.demo.dto;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO {
	
	private Long id;
	private String username;
	private String password;
	private String email;
	private String fullname;
	private Date birthday;
	private String address;
	private String phone;
	private String avatar;
	private int status;
	private Long roleID;
	private Set<RoleDTO> roles = new HashSet<>();
	private String newpassword;
	@Override
	public String toString() {
		return "UserDTO [id=" + id + ", username=" + username + ", password=" + password + ", email=" + email
				+ ", fullname=" + fullname + ", birthDay=" + birthday + ", address=" + address + ", phone=" + phone
				+ ", avatar=" + avatar + ", status=" + status + ", role=" + roleID + "]";
	}
	
}
