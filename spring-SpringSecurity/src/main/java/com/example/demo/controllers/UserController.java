package com.example.demo.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.UserDTO;
import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;
import com.example.demo.services.IUserService;

@RestController
@RequestMapping("/api/user")
public class UserController {

	@Autowired
	IUserService IUserService;
	
	@Autowired
	PasswordEncoder passwordEncoder;
	
	@Autowired
	UserRepository repository;
	

	@GetMapping("/search")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> searchUser(@RequestBody UserDTO userDTO) {
		List<UserDTO> list = IUserService.searchUser(userDTO);
		return ResponseEntity.ok(list);
	}
	
	@GetMapping("/details/{id}")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<?> getById(@PathVariable("id") Long id){
		UserDTO userDTO = IUserService.getUserById(id);
		return ResponseEntity.ok(userDTO);
	}
	
	@PutMapping("/update")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<?> updateUser(@RequestBody UserDTO userDTO){
		UserDTO dto = IUserService.updateUser(userDTO);
		return ResponseEntity.ok(dto);
	}
	@PutMapping("/status")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<?> changStatus(@RequestBody UserDTO userDTO){
		UserDTO dto = IUserService.changeStatus(userDTO);
		String mess ="";
		if(dto.getStatus() == userDTO.getStatus()) {
			 mess = "Thay đổi thành công";
		}else {
			 mess = "Thay đổi thất bai";
		}
		return ResponseEntity.ok(mess);
	}
	
	@PutMapping("/changePassword")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<?> changT(@RequestBody UserDTO userDTO) {
		UserDTO dto = IUserService.changePassword(userDTO);
		if(dto == null) {
			return ResponseEntity.ok("Mật khẩu cũ không chính xác !");
		}else {
			return ResponseEntity.ok("Đổi mật khẩu thành công");
		}
		
	}
	
	
	
	
	
	@DeleteMapping("/delete/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> deleteUser(@PathVariable("id") Long id){
		IUserService.deleteUser(id);
		return ResponseEntity.ok("Xóa Thành Công");
	}
	
	
}
