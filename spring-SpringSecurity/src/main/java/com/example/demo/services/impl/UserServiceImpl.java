package com.example.demo.services.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.demo.converter.UserConverter;
import com.example.demo.dto.UserDTO;
import com.example.demo.entity.Role;
import com.example.demo.entity.User;
import com.example.demo.payload.response.MessageResponse;
import com.example.demo.repository.UserRepository;
import com.example.demo.services.IUserService;

@Service
public class UserServiceImpl implements IUserService {

	@Autowired
	UserRepository userRepository;
	@Autowired
	UserConverter userConverter;
	@Autowired
	PasswordEncoder passwordEncoder;
	
	// Tìm kiếm
	@Override
	public List<UserDTO> searchUser(UserDTO userDTO) {

		List<User> listUser = new ArrayList<>();
		List<User> listUserByRole = new ArrayList<>();
		List<UserDTO> listUserDTO = new ArrayList<>();
		List<UserDTO> listUserDTOByRole = new ArrayList<>();
		if (userDTO.getUsername() == null) {
			userDTO.setUsername("");
		}
		if (userDTO.getFullname() == null) {
			userDTO.setFullname("");
		}
		if (userDTO.getPhone() == null) {
			userDTO.setPhone("");
		}
		if (userDTO.getEmail() == null) {
			userDTO.setEmail("");
		}
		if (userDTO.getStatus() == 0) {
			listUser = userRepository.searchUser(userDTO.getUsername(), userDTO.getFullname(), userDTO.getPhone(),
					userDTO.getEmail());
		} else {
			listUser = userRepository.searchUser(userDTO.getUsername(), userDTO.getFullname(), userDTO.getPhone(),
					userDTO.getEmail(), userDTO.getStatus());
		}

		if (userDTO.getRoleID() != null) {
			for (User u : listUser) {
				for (Role r : u.getRoles()) {
					if (r.getId() == userDTO.getRoleID()) {
						listUserByRole.add(u);
					}
				}
			}
		} else {
			for (User u : listUser) {
				listUserDTO.add(userConverter.toDTO(u));
			}
			return listUserDTO;
		}

		for (User u : listUserByRole) {
			listUserDTOByRole.add(userConverter.toDTO(u));
		}

		return listUserDTOByRole;
	}

	@Override
	public UserDTO getUserById(Long id) {
		UserDTO userDTO = new UserDTO();
		User user = new User();
		try {
			user = userRepository.getOne(id);
		} catch (Exception e) {
			return null;
		}
		
//		if(user == null) {
//			return null;
//		}
		userDTO = userConverter.toDTO(user);
		return userDTO;
	}
	
	// Update fullname, phone, adress, birthDay, avatar
	@Override
	public UserDTO updateUser(UserDTO userDTO) {
		User user = userRepository.getOne(userDTO.getId());
//		if(user == null) {
//			return null;
//		}
		user.setFullname(userDTO.getFullname());
		user.setPhone(userDTO.getPhone());
		user.setAddress(userDTO.getAddress());
		user.setBirthday(userDTO.getBirthday());
		user.setAvatar(userDTO.getAvatar());
		user = userRepository.save(user);
		return userConverter.toDTO(user);
	}
	
	
	// Thay đổi status User
	@Override
	public UserDTO changeStatus(UserDTO userDTO) {
		User user = userRepository.getOne(userDTO.getId());
		user.setStatus(userDTO.getStatus());
		user = userRepository.save(user);
		return userConverter.toDTO(user);
	}
	
	
	// Thay đổi mật khẩu User
	@Override
	public UserDTO changePassword(UserDTO userDTO) {
		String password = userRepository.getOne(userDTO.getId()).getPassword();
		String olaPassword = userDTO.getPassword();
		Boolean isPassword = passwordEncoder.matches(olaPassword, password);
		if(isPassword) {
			String newPassword = passwordEncoder.encode(userDTO.getNewpassword());
			User user = userRepository.getOne(userDTO.getId());
			user.setPassword(newPassword);
			user = userRepository.save(user);
			return userConverter.toDTO(user);
		}else {
			return null;
		}
	}

	
	
	
	
	
	
	
	
	// Xóa tài khoản
	@Override
	public void deleteUser(Long id) {
		userRepository.deleteById(id);
		
	}
	
	
	


}
