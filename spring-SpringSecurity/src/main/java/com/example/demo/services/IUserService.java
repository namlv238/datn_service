package com.example.demo.services;

import java.util.List;

import com.example.demo.dto.UserDTO;
import com.example.demo.entity.User;

public interface IUserService {
	
	// Tìm kiếm
	List<UserDTO> searchUser(UserDTO userDTO);
	
	// Lấy User thông qua ID
		UserDTO getUserById(Long id);
		
	// Update
	UserDTO updateUser(UserDTO userDTO);
	
	// Thay đổi trạng thái User
	UserDTO changeStatus(UserDTO userDTO);
	
	// Thay đổi mật khẩu của User
	UserDTO changePassword(UserDTO userDTO);
	
	
	
	
	
	
	// Xóa tài khoản
	void deleteUser(Long id);
}
