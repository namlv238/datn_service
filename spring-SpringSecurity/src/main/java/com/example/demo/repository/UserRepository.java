package com.example.demo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{
	Optional<User> findByUsername(String username);
	
	Boolean existsByUsername(String username);
	
	Boolean existsByEmail(String email);
	
	// Tìm kiếm danh sách không có có status
	@Query( value = "SELECT u.* FROM User u WHERE u.username LIKE %?1% AND u.fullname LIKE %?2% AND u.phone LIKE %?3% AND u.email LIKE %?4%", nativeQuery = true)
	public List<User> searchUser(String username, String fullname, String phone, String email);
	
	// Tìm kiếm danh sách có status
	@Query( value = "SELECT u.* FROM User u WHERE u.username LIKE %?1% AND u.fullname LIKE %?2% AND u.phone LIKE %?3% AND u.email LIKE %?4% AND u.status = ?5", nativeQuery = true)
	public List<User> searchUser(String username, String fullname, String phone, String email, int status);
	
	
}
